package todo.list.daily;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TodoListDailyApplication {

	public static void main(String[] args) {
		SpringApplication.run(TodoListDailyApplication.class, args);
	}

}
